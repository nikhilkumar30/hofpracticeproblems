const got = require("./data-1");

// 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

function countAllPeaople(got) {
  let totalCount = 0;

  got.houses.forEach((house) => {
    totalCount += house.people.length;
  });

  return totalCount;
}

const totalPeopleCount = countAllPeaople(got);
console.log("Total Count :- " + totalPeopleCount);

console.log("-----------------------------------------------");

// 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

function peopleByHouse(got) {
  const countPeopleByHouse = {};

  got.houses.forEach((house) => {
    const totalCount = house.name.length;
    countPeopleByHouse[house.name] = totalCount;
  });
  return countPeopleByHouse;
}
const totalPeopleByHouse = peopleByHouse(got);
console.log(totalPeopleByHouse);

console.log("-----------------------------------------------");

// - 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.

function everyone(got) {
  const arrayNames = [];
  got.houses.forEach((house) => {
    house.people.forEach((person) => {
      arrayNames.push(person.name);
    });
  });
  return arrayNames;
}

const namesOfEveryone = everyone(got);
console.log("All names :-");

console.log(namesOfEveryone);

console.log("----------------------------------");

// - 4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

function nameWithS(got) {
  const nameWithS = [];

  got.houses.forEach((house) => {
    house.people.forEach((person) => {
      if (person.name.toLowerCase().includes("s")) {
        nameWithS.push(person.name);
      }
    });
  });
  return nameWithS;
}

const countNameWithS = nameWithS(got);
console.log("Names having 's' & 'S' :- ");
console.log(countNameWithS);

console.log("----------------------------------");

// - 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

function nameWithA(got) {
  const nameWithA = [];

  got.houses.forEach((house) => {
    house.people.forEach((person) => {
      if (person.name.toLowerCase().includes("a")) {
        nameWithA.push(person.name);
      }
    });
  });
  return nameWithA;
}

const countNameWithA = nameWithA(got);
console.log("Names having 'a' & 'A' :- ");
console.log(countNameWithA);

console.log("----------------------------------");

// - 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

function surnameWithS(got) {
  const surNameS = [];
  got.houses.forEach((house) => {
    house.people.forEach((person) => {
      const surname = person.name.split(" ");

      if (surname[1].charAt(0) === "S") {
        surNameS.push(person.name);
      }
    });
  });
  return surNameS;
}

const surNameStartWithS = surnameWithS(got);
console.log("Surname with S :-");
console.log(surNameStartWithS);

console.log("-------------------------------------------");

// - 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).
function surnameWithA(got) {
  const surNameA = [];
  got.houses.forEach((house) => {
    house.people.forEach((person) => {
      const surname = person.name.split(" ");

      if (surname[1].charAt(0) === "A") {
        surNameA.push(person.name);
      }
    });
  });
  return surNameA;
}

const surNameStartWithA = surnameWithA(got);
console.log("Surname with A :-");
console.log(surNameStartWithA);

console.log("-------------------------------------------");

//   - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

function peopleNameOfAllHouses(got) {
  const obj = {};
  got.houses.forEach((house) => {
    const arrayNames = [];
    house.people.forEach((person) => {
      arrayNames.push(person.name);
    });

    if (!obj[house.name]) {
      obj[house.name] = arrayNames;
    }
  });
  return obj;
}

console.log(peopleNameOfAllHouses(got));
